﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {
	
	public float duration = 1f;
	public float magnitude = 1f;
	// How many frame stay on same position?
	public int framePositionDuration = 0;
	
	private Camera cam;
	private Vector3 initialCamPos;
	private Vector3 originalCamPos;
	
	private bool coroutining = false;
	private float originalZoom;
	
	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera>();
		initialCamPos = cam.transform.position;
		originalZoom = cam.orthographicSize;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnEnable()
	{
		StageEventManager.OnShake += LaunchShake;
	}
	
	void OnDisable()
	{
		StageEventManager.OnShake -= LaunchShake;
	}
	
	void LaunchShake()
	{
		StartCoroutine(Shake());
	}
	
	IEnumerator Shake() {
		
		float elapsed = 0.0f;
		
		// Not zoomed
		if (cam.orthographicSize == originalZoom)
			originalCamPos = initialCamPos;
		else
			originalCamPos = cam.transform.position;
		int i = 0;
		while (elapsed < duration) {
			
			if (i % framePositionDuration == 0)
			{
				// Have to reset every frame the position if zoomed
				if (cam.orthographicSize != originalZoom)
					originalCamPos = cam.transform.position;
				
				elapsed += Time.deltaTime;          
				
				float percentComplete = elapsed / duration;         
				float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);
				
				// map value to [-1, 1]
				float x = Random.value * 2.0f - 1.0f;
				float y = Random.value * 2.0f - 1.0f;
				x *= magnitude * damper;
				y *= magnitude * damper;
				
				cam.transform.position = new Vector3(x + originalCamPos.x,
				                                     y + originalCamPos.y,
				                                     originalCamPos.z);
			}
			yield return null;
		}
		cam.transform.position = originalCamPos;
	}
	
}

﻿using UnityEngine;
using System.Collections;

public class BossWeapon : MonoBehaviour {
    

    public void OnTriggerEnter2D(Collider2D col)
	{
		CollisionDetection(col);
	}
	
	//public void OnTriggerStay2D(Collider2D col)
	//{
	//	CollisionDetection(col);
	//}

    protected virtual void CollisionDetection(Collider2D col)
    {
        //hit player
        StageEventManager.DamageHeroEvent();
    }

    
}

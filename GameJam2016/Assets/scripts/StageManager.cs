﻿using UnityEngine;
using System.Collections;

public class StageManager : MonoBehaviour {
    public string _actualScene;
    public BossBehaviour _boss;
    public hero _hero;
    public LifeBar _lifeBar;
    public string _nextScene;

    [Header("Transforms")]
    public Transform _herosPos;
    public Transform _nextSceneSpawner;

    [Header("Transition")]
    public AnimationCurve _curveTransition;
    public float _durationTransition;

    [Header("Debug Mode")]
    public bool _DebugMode;

    private StageManager _stage;

    void Start()
    {
        if (_DebugMode)
        {
            Go();
        }
    }

    public void Go()
    {
        _hero.transform.position = _herosPos.position;
        _boss.Started = true;
        _hero.StartScene();
		_lifeBar._hero = _hero;
        _lifeBar.StartScene();
    }

    public void GoToNextScene()
    {
        StartCoroutine(PrepareNextScene());
        StartCoroutine(TransitionScene());
    }

    IEnumerator PrepareNextScene()
    {
        gameObject.name = "OldStage";
        AsyncOperation async = Application.LoadLevelAdditiveAsync(_nextScene);

        yield return async;

        _stage = GameObject.Find("Stage").GetComponent<StageManager>();
        _stage.transform.position = _nextSceneSpawner.position;
        _stage._hero = _hero;
    }

    IEnumerator TransitionScene()
    {
        while (_stage == null)
            yield return null;

        float time = Time.time;
        Transform t = _stage.GetComponent<Transform>();
        Vector3 start = t.position;

        Transform h = _hero.GetComponent<Transform>();
        Vector3 startHero = h.position;
        while (Time.time - time <= _durationTransition)
        {
            t.position = start + _curveTransition.Evaluate((Time.time - time) / _durationTransition) * (new Vector3(0,0,0) - start);
            transform.position = _curveTransition.Evaluate((Time.time - time) / _durationTransition) * (new Vector3(0, 0, 0) - start);
            h.position = Vector3.Lerp(startHero, _stage._herosPos.position, _curveTransition.Evaluate((Time.time - time) / _durationTransition));
            yield return null;
        }

        t.position = new Vector3(0, 0, 0);
        h.position = _stage._herosPos.position;

        _stage.Go();

        Destroy(gameObject);
    }
}

﻿using UnityEngine;
using System.Collections;

public class CreditsMove : MonoBehaviour {
	
	public Vector3 initialPosition;
	public Vector3 targetPosition;
	
	public float scrollSpeed = 0.3f;
	
	

	
	// Use this for initialization
	void Start () {
		
	
		
		StartCoroutine (panelMove());
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		
	}
	
	IEnumerator panelMove()
	{
		//GetComponent<RectTransform> ().position = initialPosition;
		//RectTransform transform.position = initialPosition;
		
		while (transform.position.y <  targetPosition.y) {
			
			transform.position= new Vector3 (transform.position.x ,transform.position.y + (scrollSpeed * Time.deltaTime) ,transform.position.z);
			yield return null;
		}
		transform.position= new Vector3 (targetPosition.x ,targetPosition.y,targetPosition.z);
		
		
		
		yield return null;

        Application.LoadLevel("menu");

		Destroy (gameObject);	
	}
	
	
}

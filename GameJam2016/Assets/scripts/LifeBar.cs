﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LifeBar : MonoBehaviour {

	public float horizontalOffset = 1.0f;
	public Vector3 startPosition = new Vector3(0.0f,0.0f,0.0f);

	public GameObject[] LifePointPrefabs;
	private List<LifePoint> lifePoints;

    public hero _hero;
    
    // Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StartScene() 
	{
		lifePoints = new List<LifePoint> ();
        Vector3 spawnPosition = startPosition;
        float centerOffset = (_hero.maxLife - 1) / 2 * horizontalOffset;

		for(int i = 0; i<_hero.maxLife; i++) 
		{
            spawnPosition = new Vector3(startPosition.x + i * horizontalOffset - centerOffset, startPosition.y, startPosition.z);

            GameObject go = GameObject.Instantiate(LifePointPrefabs[Random.Range(0, LifePointPrefabs.Length-1)], spawnPosition, Quaternion.identity) as GameObject;
            LifePoint obj = go.GetComponent<LifePoint>();
            obj.transform.parent = this.transform;

			lifePoints.Add(obj);
		}
	}
    

    public void getDamage()
	{
		lifePoints [_hero.life-1].Hide();
	}

    void OnEnable()
    {
        StageEventManager.DamageHero += getDamage;
    }

    void OnDisable()
    {
        StageEventManager.DamageHero -= getDamage;
    }

}

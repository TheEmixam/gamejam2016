﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class BossBehaviourKangoo : BossBehaviour {
    struct head
    {
        public bool _cry;
        public int _life;
        public int _countdown;
    }

    [Header("Kangou spec")]
    public int _CountdownHead;
    public BossVulnerability _Vulnerability;


    private head[] _Heads = new head[3];
    


	// Use this for initialization
	protected override void Start () {
        base.Start();

        _Heads[0]._cry = false;
        _Heads[0]._life = _MaxLifePoint / 3;
        _Heads[1]._cry = false;
        _Heads[1]._life = _MaxLifePoint / 3;
        _Heads[2]._cry = false;
        _Heads[2]._life = _MaxLifePoint / 3;
    }

    // Update is called once per frame
    protected override void Update () {
        base.Update();
	}

    protected override string BossMoveToTrigger(BossAttackMove attackMove)
    {
        string result = "attack_";
        switch (attackMove)
        {
            case BossAttackMove.Left:
                result += _Heads[0]._cry ? "special1" : "left";
                break;
            case BossAttackMove.Middle:
                result += _Heads[1]._cry ? "special2" : "middle";
                break;
            case BossAttackMove.Right:
                result += _Heads[2]._cry ? "special3" : "right";
                break;
            default:
                break;
        }
        return result;
    }

	void BossLaunchMoveSound(BossAttackMove attackMove)
	{
		AudioClip clip = null;
		float delay = 0f;
		switch (attackMove) {
			case BossAttackMove.Left:
                clip = _Heads[0]._cry ? audioAttackSpecial1.clip : audioAttackLeft.clip;
                delay = _Heads[0]._cry ? audioAttackSpecial1.delay : audioAttackLeft.delay;
				break;
			case BossAttackMove.Middle:
                clip = _Heads[1]._cry ? audioAttackSpecial2.clip : audioAttackMiddle.clip;
                delay = _Heads[1]._cry ? audioAttackSpecial2.delay : audioAttackMiddle.delay;
				break;
			case BossAttackMove.Right:
                clip = _Heads[2]._cry ? audioAttackSpecial3.clip : audioAttackRight.clip;
                delay = _Heads[2]._cry ? audioAttackSpecial3.delay : audioAttackRight.delay;
                break;
			default:
				break;
		}
		if (clip != null)
			StartCoroutine(PlaySoundDelay(clip, delay));
	}

    protected override IEnumerator DoPatern(Patern patern)
    {
        print("head check");
        print(_Heads[2]._life);
        for (int i = 0; i < 3; i++)
        {
            if (_Heads[i]._cry)
            {
                print("reset");
                _Heads[i]._countdown--;
                if (_Heads[i]._countdown <= 0)
                {
                    _Heads[i]._cry = false;
                    _Heads[i]._life = _MaxLifePoint / 6;
                    _LifePoint += _Heads[i]._life;
                }
            }
        }
        return base.DoPatern(patern);
    }

    public override void getDamage(bool up, float x, int damage)
    {
        if (!_isInvulnerable &&
            _Vulnerability.IsVulnerablePosition(up, x))
        {
            StartCoroutine(BlinkDamage());
            _LifePoint -= damage;

            int indice;
            if (x < 0)
            {
                indice = 0;
            }
            else if (x == 0)
                indice = 1;
            else
                indice = 2;

            _Heads[indice]._life -= damage;
            if (_Heads[indice]._life <= 0)
            {
                _Heads[indice]._cry = true;
                _Heads[indice]._countdown = _CountdownHead;
            }
            

            if (_LifePoint <= 0)
			{
            	//TODO launch dead sound
				Dead();
			}
        }
    }

    void Dead()
    {
        _StageManager.GoToNextScene();
        Destroy(gameObject);
    }

    IEnumerator BlinkDamage()
    {
        //_coroutine = true;
        float time = Time.time;
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        while (Time.time - time <= _durationBlinkDamage)
        {
            renderer.color = _CurveBlinkDamage.Evaluate((Time.time - time) / _durationBlinkDamage) == 0 ? _colorBlinkDamage : Color.white;
            yield return null;
        }

        renderer.color = Color.white;
        //_coroutine = false;
    }

    IEnumerator ChangePhase()
    {
        float time = Time.time;
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        SpriteRenderer[] weapons = GetComponentsInChildren<SpriteRenderer>();
        while (Time.time - time <= _PhaseDuration)
        {
            renderer.color = _PhaseCurve.Evaluate((Time.time - time) / _PhaseDuration) * Color.white;
            renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, 1);

            foreach (SpriteRenderer weap in weapons)
            {
                weap.color = renderer.color;
            }
            yield return null;
        }

        renderer.color = Color.white;
        foreach (SpriteRenderer weap in weapons)
        {
            weap.color = renderer.color;
        }
    }
}

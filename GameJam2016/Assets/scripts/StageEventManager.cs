﻿using UnityEngine;


public class StageEventManager : MonoBehaviour {
	
	public delegate void ShakeAction();
	public static event ShakeAction OnShake;
    
	public static void ShakeEvent()
	{
		if (OnShake != null)
			OnShake ();
	}

    public delegate void DamageBossAction(bool up, float x, int damage);
    public static event DamageBossAction DamageBoss;

    public static void DamageBossEvent(bool up, float x, int damage)
    {
        if (DamageBoss != null)
            DamageBoss(up, x, damage);
    }

    public delegate void DamageHeroAction();
    public static event DamageHeroAction DamageHero;

    public static void DamageHeroEvent()
    {
        if (DamageHero != null)
            DamageHero();
    }
}

﻿using UnityEngine;
using System.Collections;

public class Appear : MonoBehaviour {

    public AnimationCurve _CurveAlpha;
    public float _DurationAppear;
    public Color _ColorAppear;

    SpriteRenderer render;

    // Use this for initialization
    void Start () {
        render = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void Show()
    {
        StartCoroutine(ShowCoroutine());
    }

    IEnumerator ShowCoroutine()
    {
        float time = Time.time;
        while (Time.time - time <= _DurationAppear)
        {
            render.color = new Color(1, 1, 1 , _CurveAlpha.Evaluate((Time.time - time) / _DurationAppear));
            yield return null;
        }
    }
}

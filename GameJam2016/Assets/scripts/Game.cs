﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {

    void Awake()
    {
        Application.targetFrameRate = 60;
        Cursor.visible = false;
    }

    void Start()
    {
        Resources.LoadAll("");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel("menu");
        }
            
    }
}

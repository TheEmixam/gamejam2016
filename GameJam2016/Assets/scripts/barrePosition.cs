﻿using UnityEngine;
using System.Collections;

public class barrePosition : MonoBehaviour {

	[Header("Postion")]
	public Vector3 initialPosition = new Vector3(0.0f,0.0f,0.0f);

	public float scaleLenght =6.0f; // scale the bar size
	private float ratio = 1.0f;
	private int mLifePoint = 0;




	public BossBehaviour _bossBehaviour;

	// Use this for initialization
	void Start () {
		initialPosition = transform.position;
		mLifePoint = _bossBehaviour._MaxLifePoint;

		ratio = scaleLenght / mLifePoint;

		BarRescale(mLifePoint);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (mLifePoint != _bossBehaviour._LifePoint)
		{
			BarRescale (_bossBehaviour._LifePoint);

		}
	
	}


	private void BarRescale(int p_lifes)
	{	
		mLifePoint = p_lifes;

		float lifes = p_lifes * ratio;
		transform.position = new Vector3 (initialPosition.x + (lifes/2)  , transform.position.y, transform.position.z);
		transform.localScale = new Vector3 (lifes, transform.localScale.y , transform.localScale.y);

	}



}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum BossAttackMove
{
    Up,
    Down,
    Left,
    Middle,
    Right,
    Special1,
    Special2,
    Special3,
    Special4
}



[System.Serializable]
public struct Patern
{
    public float _timeBetweenAttack;
    public int _HintId;
    public List<BossAttackMove> _patern;
}

[System.Serializable]
public struct Phase
{
    public int _StartAt;
    public int _RandomChance;
    public List<Patern> _PaternList;
}

[System.Serializable]
public struct AudioClipDelay {
	public AudioClip clip;
	public float delay;
};

public class BossBehaviour : MonoBehaviour {

    [Header("Audio")]
	AudioSource audioSrc;
	public AudioClipDelay audioAttackUp;
	public AudioClipDelay audioAttackDown;
	public AudioClipDelay audioAttackLeft;
	public AudioClipDelay audioAttackRight;
	public AudioClipDelay audioAttackMiddle;
	public AudioClipDelay audioAttackSpecial1;
	public AudioClipDelay audioAttackSpecial2;
	public AudioClipDelay audioAttackSpecial3;
    public AudioClipDelay audioAttackSpecial4;
    public AudioClipDelay audioHit;
    public AudioClipDelay audioDead;

    [Header("Boss Spec")]
    public int _MaxLifePoint;
	[HideInInspector]
    public int _LifePoint;
    public Vector2 _IdleTimeMinMax;
    protected BossVulnerability bossVulnerability;

    [Header("Get damage")]
    public AnimationCurve _CurveBlinkDamage;
    public float _durationBlinkDamage;
    public Color _colorBlinkDamage;

    [Header("Links")]
    public GameObject _Weapon;
    public Animator _WeaponsAnimator;
    public StageManager _StageManager;

    [Header("Paterns")]
    public List<Phase> _PhaseList;

    [Header("Phase transition")]
    public AnimationCurve _PhaseCurve;
    public float _PhaseDuration;


    protected Animator _Animator;
    protected bool _isAttacking = false;

    protected bool _isInvulnerable = false;
    public bool isInvulnerable
    {
        get
        {
            return _isInvulnerable;
        }
    }

    protected int _PhaseIndex = 0;
    public bool _Started = false;
    public bool Started
    {
        get
        {
            return _Started;
        }
        set
        {
            _Started = value;

            if (value)
            {
                GetComponent<Animator>().SetTrigger("go");
            }
        }
    }
	// Use this for initialization
	protected virtual void Start () {
        _LifePoint = _MaxLifePoint;
        _Animator = GetComponent<Animator>();
		audioSrc = GetComponent<AudioSource>();
        bossVulnerability = GetComponent<BossVulnerability>();
    }

// Update is called once per frame
    protected virtual void Update()
    {
        if (_Started)
        {
            if (!_isAttacking &&
                !_Animator.GetCurrentAnimatorStateInfo(0).IsName("boss_damage_up") &&
                !_Animator.GetCurrentAnimatorStateInfo(0).IsName("boss_damage_down"))
            {
                StartCoroutine(DoPatern(_PhaseList[_PhaseIndex]._PaternList[Random.Range(0, _PhaseList[_PhaseIndex]._PaternList.Count)]));
            }
        }
	}

    protected virtual string BossMoveToTrigger(BossAttackMove attackMove)
    {

        string result = "attack_";
        switch (attackMove)
        {
            case BossAttackMove.Up:
                result += "up";
                break;
            case BossAttackMove.Down:
                result += "down";
                break;
            case BossAttackMove.Left:
                result += "left";
                break;
            case BossAttackMove.Middle:
                result += "middle";
                break;
            case BossAttackMove.Right:
                result += "right";
                break;
            case BossAttackMove.Special1:
                result += "special1";
                break;
            case BossAttackMove.Special2:
                result += "special2";
                break;
            case BossAttackMove.Special3:
                result += "special3";
                break;
            case BossAttackMove.Special4:
                result += "special4";
                break;
            default:
                break;
        }
        return result;
    }

    protected virtual void BossLaunchMoveSound(BossAttackMove attackMove)
	{
		AudioClip clip = null;
		float delay = 0f;
		switch (attackMove) {
			case BossAttackMove.Up:
				clip = audioAttackUp.clip;
				delay = audioAttackUp.delay;
				break;
			case BossAttackMove.Down:
				clip = audioAttackDown.clip;
				delay = audioAttackDown.delay;
				break;
			case BossAttackMove.Left:
				clip = audioAttackLeft.clip;
				delay = audioAttackLeft.delay;
				break;
			case BossAttackMove.Middle:
				clip = audioAttackMiddle.clip;
				delay = audioAttackMiddle.delay;
				break;
			case BossAttackMove.Right:
				clip = audioAttackRight.clip;
				delay = audioAttackRight.delay;
				break;
			case BossAttackMove.Special1:
				clip = audioAttackSpecial1.clip;
				delay = audioAttackSpecial1.delay;
				break;
			case BossAttackMove.Special2:
				clip = audioAttackSpecial2.clip;
				delay = audioAttackSpecial2.delay;
				break;
			case BossAttackMove.Special3:
				clip = audioAttackSpecial3.clip;
				delay = audioAttackSpecial3.delay;
				break;
            case BossAttackMove.Special4:
                clip = audioAttackSpecial4.clip;
                delay = audioAttackSpecial4.delay;
                break;
			default:
				break;
		}
		if (clip != null)
			StartCoroutine(PlaySoundDelay(clip, delay));
	}

    protected IEnumerator PlaySoundDelay(AudioClip clip, float delay)
	{
		yield return new WaitForSeconds (delay);
		audioSrc.PlayOneShot (audioAttackUp.clip);
	}

    protected virtual IEnumerator DoPatern(Patern patern)
    {
        _isAttacking = true;

        _Animator.SetBool("attack", true);
        _Animator.SetInteger("hint", patern._HintId);

        while (!_Animator.GetCurrentAnimatorStateInfo(0).IsName("boss_attacking " + (_PhaseIndex+1)))
            yield return null;

        //_isInvulnerable = true;

        int i=0;
        foreach (BossAttackMove attackMove in patern._patern)
        {
            _WeaponsAnimator.SetTrigger(BossMoveToTrigger(attackMove));
			BossLaunchMoveSound(attackMove);
            print(BossMoveToTrigger(attackMove));

            while (!_WeaponsAnimator.GetCurrentAnimatorStateInfo(0).IsName(BossMoveToTrigger(attackMove)))
            {
                yield return null;
            }
            while (_WeaponsAnimator.GetCurrentAnimatorStateInfo(0).IsName(BossMoveToTrigger(attackMove)))
            {
                yield return null;
            }

            if (i == patern._patern.Count - 1)
                yield return new WaitForSeconds(patern._timeBetweenAttack);
            i++;
        }

        int lastIndex = _PhaseIndex;
        for (int e = 0; e < _PhaseList.Count; e++)
        {
            if (_LifePoint <= _PhaseList[e]._StartAt)
                _PhaseIndex = e;
            else
                break;
        }
        for (int e = 0; e < _PhaseList.Count; e++)
        {
            if(_PhaseList[e]._RandomChance>0)
            {
                int rand = Random.Range(1, _PhaseList[e]._RandomChance + 1);
                _PhaseIndex = rand == 1 ? e : 0;
            }
        }
        if (_PhaseIndex != lastIndex)
            StartCoroutine(ChangePhase());
        _Animator.SetInteger("phase", _PhaseIndex + 1);

        _Animator.SetBool("attack", false);

        //_isInvulnerable = false;
        _Animator.SetBool("idle", true);

        yield return new WaitForSeconds(_IdleTimeMinMax.x);


        _Animator.SetBool("idle", false);

        _isAttacking = false;
    }

    void OnEnable()
    {
        StageEventManager.DamageBoss += getDamage;
    }

    void OnDisable()
    {
        StageEventManager.DamageBoss -= getDamage;
    }

    public virtual void getDamage(bool up, float x, int damage)
    {
        if (!_isInvulnerable &&
            bossVulnerability.IsVulnerablePosition(up, x))
        {

            StartCoroutine(BlinkDamage());
            _LifePoint -= damage;

            if (_LifePoint <= 0)
			{
            	//TODO launch dead sound
				audioSrc.PlayOneShot (audioDead.clip);
				Dead();
			}
			else
			{
				audioSrc.PlayOneShot (audioHit.clip);
			}
        }
    }

    protected void Dead()
    {
		if (this.name == "Boss_Koala")
			Application.LoadLevel ("generique_fin");
        _StageManager.GoToNextScene();
        Destroy(gameObject);
    }

    protected IEnumerator BlinkDamage()
    {
        //_coroutine = true;
        float time = Time.time;
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        while (Time.time - time <= _durationBlinkDamage)
        {
            renderer.color = _CurveBlinkDamage.Evaluate((Time.time - time) / _durationBlinkDamage) == 0 ? _colorBlinkDamage : Color.white;
            yield return null;
        }

        renderer.color = Color.white;
        //_coroutine = false;
    }

    protected virtual IEnumerator ChangePhase()
    {
        float time = Time.time;
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        SpriteRenderer[] weapons = GetComponentsInChildren<SpriteRenderer>();
        while (Time.time - time <= _PhaseDuration)
        {
            renderer.color = _PhaseCurve.Evaluate((Time.time - time) / _PhaseDuration) * Color.white;
            renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, 1);

            foreach (SpriteRenderer weap in weapons)
            {
                weap.color = renderer.color;
            }
            yield return null;
        }

        renderer.color = Color.white;
        foreach (SpriteRenderer weap in weapons)
        {
            weap.color = renderer.color;
        }
    }
}

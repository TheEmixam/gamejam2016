﻿using UnityEngine;
using System.Collections;

public class menu_background : MonoBehaviour {
    public Transform _Destination;
    public AnimationCurve _Curve;
    public float _DurationTransition;
    public Transform _ParentNextScene;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //if (Input.GetKeyDown(KeyCode.Mouse0))
        //    StartCoroutine(Transtition());

	}

    public void launch()
    {
        StartCoroutine(Transtition());
    }

    public IEnumerator Transtition()
    {
        float time = Time.time;
        Transform t = GetComponent<Transform>();
        Vector3 start = t.position;
        while (Time.time - time <= _DurationTransition)
        {
            t.position = start + _Curve.Evaluate((Time.time - time) / _DurationTransition) * (_Destination.transform.position - start);
            yield return null;
        }

        t.position = _Destination.transform.position;

        //Application.LoadLevel("stage1");
    }
}

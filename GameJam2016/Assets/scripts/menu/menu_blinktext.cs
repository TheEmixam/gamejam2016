﻿using UnityEngine;
using System.Collections;

public class menu_blinktext : MonoBehaviour {
    public AnimationCurve _Curve;
    public float _duration;

    public bool alwaysBlink;

    public void Start()
    {
        if (alwaysBlink)
            StartCoroutine(Blink());
    }

    public void launch()
    {
        alwaysBlink = false;
        StartCoroutine(Disapear());
    }

    IEnumerator Blink()
    {
        float time = Time.time;
        TextMesh t = GetComponent<TextMesh>();
        while (alwaysBlink)
        {
            t.color = new Color(1,1,1, _Curve.Evaluate((Time.time - time) / _duration));
            yield return null;
        }
    }


    IEnumerator Disapear()
    {
        float time = Time.time;
        TextMesh t = GetComponent<TextMesh>();
        while (Time.time - time <= _duration)
        {
            t.color = new Color(1, 1, 1, _Curve.Evaluate((Time.time - time) / _duration));
            yield return null;
        }

        t.color = new Color(1, 1, 1, 0);
    }
}

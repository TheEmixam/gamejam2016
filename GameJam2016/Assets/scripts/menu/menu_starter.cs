﻿using UnityEngine;
using System.Collections;

public class menu_starter : MonoBehaviour {
    public string _firstStage;
    public menu_background _background;
    public menu_blinktext[] _startTitle;
    public GameObject _menuGlobal;
    public Appear _keys;
    public GameObject _fakeHero;
    private StageManager _stage;

    public AudioClip GameMusic;

    private bool started = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (!started &&
            Input.GetKeyDown(KeyCode.Return))
        {
            StartCoroutine(Launch());
            StartCoroutine(PrepareNextScene());
        }
	}

    IEnumerator PrepareNextScene()
    {
        AsyncOperation async = Application.LoadLevelAdditiveAsync(_firstStage);

        yield return async;

        _stage = GameObject.Find("Stage").GetComponent<StageManager>();
        _stage.transform.parent = _background._ParentNextScene;
        _stage.transform.localPosition = new Vector3(0, 0, 0);
    }

    IEnumerator Launch()
    {
        started = true;
        foreach (menu_blinktext blink in _startTitle)
        {
            GameObject.Destroy(blink.gameObject);
        }

        yield return new WaitForSeconds(0.4f);

        _fakeHero.SetActive(true);
        _background.launch();
        Invoke("ShowKeys", 8f);
        yield return new WaitForSeconds(_background._DurationTransition);
        FindObjectOfType<Music>().SetMusic(GameMusic);
        _stage.transform.parent = null;
        _stage.transform.position = new Vector3(0, 0, 0);

        _stage.Go();

        _menuGlobal.SetActive(false);
    }

    void ShowKeys()
    {
        _keys.Show();
    }
}


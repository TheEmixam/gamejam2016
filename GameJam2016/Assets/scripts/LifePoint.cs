﻿using UnityEngine;
using System.Collections;

public class LifePoint : MonoBehaviour {

	Animator anim;
	// Use this for initialization
	void Start () {
	
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Hide()
	{
		anim.SetBool("visible",false);
	}
	public void Show()
	{
		anim.SetBool("visible",true);
	}

}

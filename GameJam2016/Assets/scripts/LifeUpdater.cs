﻿using UnityEngine;
using System.Collections;

public class LifeUpdater : MonoBehaviour {
    public AnimationCurve _Curve;
    public float _BlinkDuration;
    private bool _coroutine = false;

    void OnEnable()
    {
        StageEventManager.DamageHero += getDamage;
    }

    void OnDisable()
    {
        StageEventManager.DamageHero -= getDamage;
    }

    public void getDamage()
    {
        if(!_coroutine)
            StartCoroutine(Blink());
    }

    IEnumerator Blink()
    {
        _coroutine = true;
        float time = Time.time;
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        while (Time.time - time <= _BlinkDuration)
        {
            renderer.color = new Color(_Curve.Evaluate((Time.time - time) / _BlinkDuration),
                _Curve.Evaluate((Time.time - time) / _BlinkDuration),
                _Curve.Evaluate((Time.time - time) / _BlinkDuration),1);
            yield return null;
        }
        _coroutine = false;
    }
}

﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {

    AudioSource audioSource;

	// Use this for initialization
	void Start () {
        GameObject.DontDestroyOnLoad(this);
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    public void SetMusic(AudioClip audioClip)
    {
        audioSource.clip = audioClip;
        audioSource.Play();
    }
}

using UnityEngine;
using System.Collections;

public class hero : MonoBehaviour {

    Animator anim;

	AudioSource audioSrc;

    public bool Playable = false;

	[Header("Life")]
    public AudioClip audioHit;
    public int life = 7;
    public int maxLife = 7;
    public bool alive = true;
    
	[Header("Move")]
	public AudioClip audioMove ;
    public float speed = 1.0f;
    public float minPosX = -2.0f;
    public float maxPosX = 2.0f;

	[Header("Attack up")]
	public AudioClip audioAttackUp ;
	public float attackUpOffset = 1.0f;
    public GameObject attackUpPrefab;
    public float attackUpRange = 1f;
	
	[Header("Attack down")]
	public float attackDownOffset = 1.0f;
	public GameObject attackDownPrefab;
    public float attackDownRange = 1f;
	
	[Header("Attack special")]
	public float attackSpecialOffset = 1.0f;
	public GameObject attackSpecialPrefab;
	public float attackSpecialRange = 1f;

    int position = 0; // left:-1 start=middle:0 right:1

    bool coroutining = false;
    
    // Use this for initialization
	void Start () {
        life = maxLife;
		anim = GetComponent<Animator>();
		audioSrc = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update () 
    {

        if (!coroutining && alive && Playable)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                StartCoroutine(Duck());
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
               StartCoroutine(Jump());
            }


            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (position != -1)
                {
                      StartCoroutine( MoveLeft());
                }
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (position != 1)
                {
                       StartCoroutine(MoveRight());
                }
            }


            //else if (Input.GetKeyDown(KeyCode.X) && Input.GetKeyDown(KeyCode.C))
            //{
            //    StartCoroutine(AttackSpecial());
            //}
            else if (Input.GetKeyDown(KeyCode.C))
            {
                StartCoroutine(AttackDown());
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                StartCoroutine(AttackUp());
            }
         
        }

        if (life <= 0 && alive)
        {
            Dead();
        }
    
	}


    IEnumerator MoveRight()
    {
        coroutining = true;

        anim.SetBool("moving", true);
		
        float target = 0.0f;
		transform.localScale = new Vector3 (1, 1, 1);
        if (position == 0)
        {
            target = maxPosX;
        }
		while (anim.GetCurrentAnimatorStateInfo(0).IsName("hero_idle"))
		{
			yield return null;
		}

		audioSrc.PlayOneShot(audioMove);

        while (transform.position.x < target)
        {
            transform.position = new Vector3(transform.position.x + (speed * Time.deltaTime), transform.position.y, transform.position.z);
            yield return null;
        }

        position += 1;

        anim.SetBool("moving", false);
        transform.position = new Vector3(target, transform.position.y, transform.position.z);

        coroutining = false;
    }
    
    IEnumerator MoveLeft()
    {
        coroutining = true;
        
        anim.SetBool("moving", true);
        
        float target = 0.0f;

		transform.localScale = new Vector3 (-1, 1, 1);

        if (position == 0)
        {
            target = minPosX;
        }
		while (anim.GetCurrentAnimatorStateInfo(0).IsName("hero_idle"))
		{
			yield return null;
		}
		audioSrc.PlayOneShot(audioMove);
        while (transform.position.x > target)
        {
            transform.position = new Vector3(transform.position.x  - (speed * Time.deltaTime), transform.position.y, transform.position.z);
            yield return null;
        }


        position -= 1;

        transform.position = new Vector3(target, transform.position.y, transform.position.z);
        anim.SetBool("moving", false);

        coroutining = false;
    }

    IEnumerator Duck() 
    {
        coroutining = true;

        anim.SetBool("ducked",true);
		while (anim.GetCurrentAnimatorStateInfo(0).IsName("hero_idle"))
		{
			yield return null;
		}
		audioSrc.PlayOneShot(audioMove);
        while (Input.GetKey(KeyCode.DownArrow))
        {
            yield return null;
        }
        anim.SetBool("ducked", false);

        while (!anim.GetCurrentAnimatorStateInfo(0).IsName("hero_idle"))
        {
            yield return null;
        }

        coroutining = false;
    }

    IEnumerator Jump()
    {
        coroutining = true;
        anim.SetTrigger("jump");
		while (anim.GetCurrentAnimatorStateInfo(0).IsName("hero_idle"))
		{
			yield return null;
		}
		audioSrc.PlayOneShot(audioMove);
        while (!anim.GetCurrentAnimatorStateInfo(0).IsName("hero_idle"))
        {
            yield return null;
        }

        coroutining = false;
    }

    IEnumerator AttackUp()
    {
        coroutining = true;
        StageEventManager.DamageBossEvent(true, transform.position.x, 2);
		
		var someRandomPoint = Random.insideUnitCircle * attackUpRange + new Vector2(transform.position.x, attackUpOffset);

        anim.SetTrigger("attack_up");
		Instantiate (attackUpPrefab, someRandomPoint,Quaternion.identity);
		while (anim.GetCurrentAnimatorStateInfo(0).IsName("hero_idle"))
		{
			yield return null;
		}
		audioSrc.PlayOneShot(audioAttackUp);

        while (!anim.GetCurrentAnimatorStateInfo(0).IsName("hero_idle"))
        {
            yield return null;
        }



        coroutining = false;
    }

    IEnumerator AttackDown()
    {
        coroutining = true;
        StageEventManager.DamageBossEvent(false, transform.position.x, 2);

		var someRandomPoint = Random.insideUnitCircle * attackDownRange + new Vector2(transform.position.x, attackDownOffset);
		
        anim.SetTrigger("attack_down");
		Instantiate (attackDownPrefab, someRandomPoint,Quaternion.identity);
		while (anim.GetCurrentAnimatorStateInfo(0).IsName("hero_idle"))
		{
			yield return null;
		}
		audioSrc.PlayOneShot(audioAttackUp);
		while (!anim.GetCurrentAnimatorStateInfo(0).IsName("hero_idle"))
        {
            yield return null;
        }

        coroutining = false;
    }

    IEnumerator AttackSpecial()
    {
        coroutining = true;
		StageEventManager.ShakeEvent ();
		
		var someRandomPoint = Random.insideUnitCircle * attackSpecialRange + new Vector2(transform.position.x, attackSpecialOffset);
		
        anim.SetTrigger("attack_special");
		Instantiate (attackSpecialPrefab, someRandomPoint, Quaternion.identity);
		while (anim.GetCurrentAnimatorStateInfo(0).IsName("hero_idle"))
		{
			yield return null;
		}
		audioSrc.PlayOneShot(audioAttackUp);
		while (!anim.GetCurrentAnimatorStateInfo(0).IsName("hero_idle"))
        {
            yield return null;
        }

        coroutining = false;
    }

    public void StartScene()
    {
        gameObject.SetActive(true);
        life = maxLife;
        Playable = true;
        DestroyFakeHero();
    }

    void DestroyFakeHero()
    {
        FakeHero fakeHero = FindObjectOfType<FakeHero>();
        if (fakeHero != null)
            GameObject.Destroy(fakeHero.gameObject);
    }

    void Dead()
    {
        alive = false;
        anim.SetTrigger("dead");
		Application.LoadLevel ("generique_fin");
		Debug.Log ("hello");
    }

    void getDamage()
    {
        life--;
        audioSrc.PlayOneShot(audioHit);

        if (life <= 0)
        {
            Dead();
        }
    }

    void OnEnable()
    {
        StageEventManager.DamageHero += getDamage;
    }

    void OnDisable()
    {
        StageEventManager.DamageHero -= getDamage;
    }

}
